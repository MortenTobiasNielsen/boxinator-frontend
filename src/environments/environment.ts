// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  boxinatorAPI: 'https://boxinatorwebapi.azurewebsites.net/api/v1/',
  userSessionName: 'UserSession',
  auth: {
    domain: 'dev-31rrv7wg.us.auth0.com',
    clientId: 'gsfeJyWBLZQfGFrkrTwejKsdxqfvrVhF',
    redirectUri: window.location.origin,
    audience: 'https://www.boxinatorwebapi.com',
    boxinatorAuthApi: 'https://dev-31rrv7wg.us.auth0.com/oauth/token',
    managmentApiReqBody:
      '{"client_id":"gl8NM4sZySvefhJTCdjlZELsxTWuPB2z","client_secret":"IWvvzjSnABTVCb-9kU8MIXdx5ytmoUHPlNbKuu5GOV4kmFB07q0g5EYfCyVJbuuF","audience":"https://dev-31rrv7wg.us.auth0.com/api/v2/","grant_type":"client_credentials"}',
    boxinatorWebApiReqBody:
      '{"client_id":"YBDZEfYsvtVjqA38ZATImBEW1nXF0HJL","client_secret":"1DUvxNYctsZbYWRqzR98_KyoodHdhF1UBUByf56Bh-qYlkiwUgZdN7SJRKy5-0za","audience":"https://www.boxinatorwebapi.com","grant_type":"client_credentials"}',
    auth0UserDatabase: 'Username-Password-Authentication',
    auth0UsersApi: 'https://dev-31rrv7wg.us.auth0.com/api/v2/users',
  },
  dev: {
    serverUrl: 'https://localhost:44313',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
