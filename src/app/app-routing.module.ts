import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './pages/register/register.page';
import { AccountComponent } from './pages/account/account.page';
import { MainComponent } from './pages/main/main.page';
import { SettingsComponent } from './pages/settings/settings.page';
import { NotFoundComponent } from './pages/not-found/not-found.page';
import { AuthGuard } from './guards/auth.guard';
import { AuthLevels } from './utils/enums';
import { ShipmentsComponent } from './pages/shipments/shipments.page';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: [AuthGuard],
    data: { authLevel: AuthLevels.GUEST },
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [AuthGuard],
    data: { authLevel: AuthLevels.GUEST },
  },
  {
    path: 'shipments',
    component: ShipmentsComponent,
    canActivate: [AuthGuard],
    data: { authLevel: AuthLevels.GUEST },
  },
  {
    path: 'account',
    component: AccountComponent,
    canActivate: [AuthGuard],
    data: { authLevel: AuthLevels.USER },
  },
  {
    path: 'settings',
    component: SettingsComponent,
    canActivate: [AuthGuard],
    data: { authLevel: AuthLevels.ADMIN },
  },
  {
    path: '**',
    pathMatch: 'full',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
