import { Inject, Injectable } from '@angular/core';
import { AuthLevels } from '../utils/enums';
import { UserSession } from '../models/user.model';
import { Account } from '../models/account.model';
import { environment } from 'src/environments/environment';
import { AuthService } from '@auth0/auth0-angular';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Auth0User } from '../models/auth0-user.model';

import { CookieService } from 'ngx-cookie-service';
import { User } from '../models/user';
import { boxinatorUser } from '../models/boxinatorUser';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  private userSessionInitialState: UserSession = {
    id: -1,
    name: '',
    authLevel: AuthLevels.GUEST,
  };

  user: any;

  authLevelName = 'CurrentAuthLevel';

  sessionLoading = true;

  private _userSession: UserSession = this.userSessionInitialState;

  constructor(
    public auth: AuthService,
    @Inject(DOCUMENT) private doc: Document,
    private _router: Router,
    private http: HttpClient,
    private cookieService: CookieService
  ) {
    const maybeAuthLevel = localStorage.getItem(this.authLevelName);

    if (maybeAuthLevel) {
      this._userSession.authLevel = +maybeAuthLevel;
    }

    const storedUser = localStorage.getItem(environment.userSessionName);
    if (storedUser) {
      this._userSession = JSON.parse(storedUser) as UserSession;
    }

    this.auth.user$.subscribe((profile) => {
      if (profile) {
        this._userSession.authLevel = AuthLevels.USER;

        if (profile?.email! === 'admintest@boxinator.com') {
          this._userSession.authLevel = AuthLevels.ADMIN;
        }
      }

      localStorage.setItem(
        this.authLevelName,
        this._userSession.authLevel.toString()
      );
      this.sessionLoading = false;
    });
  }

  get userSession(): UserSession {
    return this._userSession;
  }

  public setUserInfo(account_info: Account): void {
    this._userSession = { ...this._userSession, ...account_info };
    this.setSession(this._userSession);
  }

  public setAuthLevel(authLevel: AuthLevels): void {
    this._userSession = { ...this._userSession, authLevel: authLevel };
    this.setSession(this._userSession);
  }

  private setSession(userSession: UserSession): void {
    localStorage.setItem(
      environment.userSessionName,
      JSON.stringify(userSession)
    );
  }

  fetchManagementToken(): void {
    const headers = { 'content-type': 'application/json' };
    const body = environment.auth.managmentApiReqBody;
    this.http
      .post<any>(environment.auth.boxinatorAuthApi, body, { headers })
      .subscribe((data) => {
        this.cookieService.set('managementToken', data['access_token']);
      });
  }

  fetchBoxinatorWebApiToken(): void {
    const headers = { 'content-type': 'application/json' };
    const body = environment.auth.boxinatorWebApiReqBody;
    this.http
      .post<any>(environment.auth.boxinatorAuthApi, body, { headers })
      .subscribe((data) => {
        this.cookieService.set('boxinatorWebApiToken', data['access_token']);
      });
  }

  fetchUserProfile(): void {
    this.auth.user$.subscribe((data) => {
      this.cookieService.set('userProfile', JSON.stringify(data));
      this._userSession.authLevel = AuthLevels.USER;
      if (data?.email! === 'admintest@boxinator.com') {
        this._userSession.authLevel = AuthLevels.ADMIN;
      }
    });
  }

  public loginWithRedirect(): void {
    this.auth
      .loginWithRedirect({
        appState: {
          target: '/shipments',
        },
      })
      .subscribe(() => {});
  }

  logout(): void {
    this.cookieService.delete('userProfile');
    this.auth.logout({
      returnTo: this.doc.location.origin,
      client_id: environment.auth.clientId,
    });
    localStorage.removeItem(this.authLevelName);
    this._userSession.authLevel = AuthLevels.GUEST;

    this._router.navigate(['/']);
  }

  createUserInApi(user: boxinatorUser) {
    const headers = {
      'content-type': 'application/json',
    };
    const boxUser: boxinatorUser = {
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      address: user.address,
      phoneNumber: user.phoneNumber,
      countryId: user.countryId,
      dateOfBirth: user.dateOfBirth + 'T00:00:00.00Z',
    };

    return this.http.post(environment.boxinatorAPI + '/Users', boxUser, {
      headers,
    });
  }

  register(user: User) {
    const token = this.cookieService.get('managementToken');
    const headers = {
      'content-type': 'application/json',
      Authorization: 'Bearer ' + token,
    };
    let newUser = new Auth0User();
    newUser.given_name = user.firstName;
    newUser.family_name = user.lastName;
    newUser.name = user.firstName + ' ' + user.lastName;
    newUser.email = user.email;
    newUser.email_verified = false;
    newUser.connection = environment.auth.auth0UserDatabase;
    newUser.password = user.password;

    return this.http.post(environment.auth.auth0UsersApi, newUser, { headers });
  }
}
