import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Shipment } from '../models/shipment.model';

@Injectable({
  providedIn: 'root',
})
export class TierService {
  private TierApiPath = environment.boxinatorAPI + 'Tiers';
  constructor(private readonly http: HttpClient) {}

  public GetAllTier(): Observable<Shipment[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.get<any[]>(this.TierApiPath, httpOptions);
  }
}
