import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ShipmentHistory } from '../models/shipment-history';
import { NewShipment, Shipment } from '../models/shipment.model';

@Injectable({
  providedIn: 'root',
})
export class ShipmentService {
  private shipmentApiPath = environment.boxinatorAPI + 'shipments';
  private _error: string = '';

  constructor(private readonly http: HttpClient) {}

  get error(): string {
    return this._error;
  }

  public GetFlatFee(): Observable<number> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.get<number>(
      this.shipmentApiPath + '/flatfee',
      httpOptions
    );
  }

  public GetAllActive(email: string): Observable<Shipment[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.get<Shipment[]>(
      this.shipmentApiPath + '/active/' + email,
      httpOptions
    );
  }

  public GetAllCompleted(email: string): Observable<Shipment[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.get<Shipment[]>(
      this.shipmentApiPath + '/completed/' + email,
      httpOptions
    );
  }

  public GetAllCancelled(email: string): Observable<Shipment[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.get<Shipment[]>(
      this.shipmentApiPath + '/cancelled/' + email,
      httpOptions
    );
  }

  public getById(id: number): Observable<Shipment> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.get<Shipment>(
      `${this.shipmentApiPath}/${id}`,
      httpOptions
    );
  }

  public GetHistory(id: number): Observable<ShipmentHistory[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.get<ShipmentHistory[]>(
      `${this.shipmentApiPath}/history/${id}`,
      httpOptions
    );
  }

  public getAllByCustomerId(id: number): Observable<Shipment[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.get<Shipment[]>(
      `${this.shipmentApiPath}/customer/${id}`,
      httpOptions
    );
  }

  public create(shipment: NewShipment): Observable<Shipment> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.post<Shipment>(
      `${this.shipmentApiPath}`,
      shipment,
      httpOptions
    );
  }

  public update(shipment: Shipment): Observable<Shipment> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.put<Shipment>(
      `${this.shipmentApiPath}/${shipment.shipmentId}`,
      {
        shipmentId: shipment.shipmentId,
        shipmentStatusId: shipment.shipmentStatusId,
      },
      httpOptions
    );
  }

  public delete(id: number): Observable<void> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.delete<void>(`${this.shipmentApiPath}/${id}`, httpOptions);
  }
}
