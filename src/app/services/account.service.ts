import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Account } from '../models/account.model';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  private accountApiPath = environment.boxinatorAPI + 'users/';

  private _error: string = '';

  constructor(private readonly http: HttpClient) {}

  get error(): string {
    return this._error;
  }

  public getById(id: number): Observable<Account> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.get<Account>(this.accountApiPath + id, httpOptions);
  }

  public getByEmail(email: string): Observable<Account> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.get<Account>(
      this.accountApiPath + 'email/' + email,
      httpOptions
    );
  }

  public create(account: Account): Observable<Account> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.post<Account>(this.accountApiPath, account, httpOptions);
  }

  public Update(id: number, account: Account): Observable<Account> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.put<Account>(
      `${this.accountApiPath}${id}`,
      account,
      httpOptions
    );
  }
}
