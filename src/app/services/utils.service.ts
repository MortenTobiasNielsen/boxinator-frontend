import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ShipmentStatus } from '../models/shipment-status.model';
import { Tier } from '../models/tier.model';

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  private _error: string = '';

  constructor(private readonly http: HttpClient) {}

  get error(): string {
    return this._error;
  }

  public GetTiers(): Observable<Tier[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };

    return this.http.get<Tier[]>(
      environment.boxinatorAPI + 'tiers',
      httpOptions
    );
  }

  public GetShipmentStatuses(): Observable<ShipmentStatus[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };

    return this.http.get<ShipmentStatus[]>(
      environment.boxinatorAPI + 'shipmentstatuses',
      httpOptions
    );
  }
}
