import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CountryCost } from '../models/country-cost.model';

@Injectable({
  providedIn: 'root',
})
export class CountryCostService {
  countryCostApiPath = environment.boxinatorAPI + 'settings/countries/';

  private _countryCost: CountryCost[] = [];
  private _error: string = '';

  constructor(private readonly http: HttpClient) {}

  get countryCost(): CountryCost[] {
    return this._countryCost;
  }

  get error(): string {
    return this._error;
  }

  public getAll(): Observable<CountryCost[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    return this.http.get<CountryCost[]>(this.countryCostApiPath, httpOptions);
  }

  public create(country: string, multiplier: number): Observable<CountryCost> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.post<CountryCost>(
      this.countryCostApiPath,
      { name: country, multiplier: multiplier },
      httpOptions
    );
  }

  public Update(countyCost: CountryCost): Observable<CountryCost> {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'X-API-Key': environment.apiKey, //TODO: What is this supposed to be?
        'Content-Type': 'application/json',
      }),
    };

    return this.http.put<CountryCost>(
      `${this.countryCostApiPath}${countyCost.countryId}`,
      countyCost,
      httpOptions
    );
  }
}
