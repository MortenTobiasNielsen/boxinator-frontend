export interface ShipmentHistory {
  shipmentStatusId: number;
  status: string;
  statusChangeDateTime: Date;
  statusChangeDateTimeFormatted: string;
}
