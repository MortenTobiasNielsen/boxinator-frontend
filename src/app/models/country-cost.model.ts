export interface CountryCost {
  countryId: number,
  name: string,
  multiplier: number,
}
