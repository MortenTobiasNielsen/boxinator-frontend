import { ShipmentStatus } from './shipment-status.model';

// TODO: Do we want the admin to be able to get use info from a shipment?
export interface Shipment {
  shipmentId?: number;
  receiverName: string;
  address: string;
  country: string;
  shipmentStatusId: number;
  tierId: number;
  hexColor: string;
  price: number;
}

export interface Shipment {
  tier: string;
  shipmentStatuses: ShipmentStatus[];
}

export interface NewShipment {
  receiverName: string;
  address: string;
  hexColor: string;
  tierId: number;
  country: string;
  price: number;
  userEmail: string;
}
