export interface ShipmentStatus {
  shipmentStatusId: number;
  status: string;
}
