export class Auth0User {
  email: string;
  email_verified: false;
  given_name: string;
  family_name: string;
  name: string;
  connection: string;
  password: string;

  constructor() {}
}
