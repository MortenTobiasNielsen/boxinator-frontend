export class User {
  firstName: string;
  lastName: string;
  dob: string;
  email: string;
  password: string;
  address: string;
  countryOfResidence: string;
  zipCode: string;
  phoneNumber: string;
  constructor() {}
}
