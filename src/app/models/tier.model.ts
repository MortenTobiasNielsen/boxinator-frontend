export interface Tier {
  tierId: number;
  name: string;
  weight: number;
}
