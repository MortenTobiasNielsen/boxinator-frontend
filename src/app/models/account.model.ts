export interface Account {
  userId: number;
  firstName: string;
  lastName: string;
  email: string;
  dateOfBirth: Date;
  countryId: string;
  zipCode: string;
  contactNumber: string;
  address: string;
}
