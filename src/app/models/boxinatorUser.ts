export interface boxinatorUser {
  firstName: string;
  lastName: string;
  dateOfBirth: string;
  email: string;
  address: string;
  countryId: string;
  phoneNumber: string;
}
