import { AuthLevels } from '../utils/enums';

export interface UserSession {
  id: number;
  name: string;
  authLevel: AuthLevels;
}

export interface UserInfo {
  UserId: number;
  FirstName: string;
  LastName: string;
  DateOfBirth: string;
  PhoneNumber: number;
  Address: string;
  CountryId: number;
  email: string;
}
