/**
 The list of authentication levels should always go from lowest to highest authentication. 
 */
export enum AuthLevels {
  GUEST,
  USER,
  ADMIN,
}
