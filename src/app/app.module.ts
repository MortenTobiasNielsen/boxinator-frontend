import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './pages/register/register.page';
import { AccountComponent } from './pages/account/account.page';
import { MainComponent } from './pages/main/main.page';
import { SettingsComponent } from './pages/settings/settings.page';
import { NotFoundComponent } from './pages/not-found/not-found.page';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ColorSketchModule } from 'ngx-color/sketch';
import { LandingComponent } from '../app/components/nav-bar/nav-bar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AuthHttpInterceptor, AuthModule } from '@auth0/auth0-angular';
import { environment as env } from '../environments/environment';
import { ColorPickerModule } from 'ngx-color-picker';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { RegistrationFormComponent } from './components/registration-form/registration-form.component';
import { LoadingComponent } from './components/loading/loading.component';
import { ShipmentsComponent } from './pages/shipments/shipments.page';
import { SearchComponent } from './components/search/search.component';
import { SearchFilterPipe } from './pipe/search-filter.pipe';
import { CookieService } from 'ngx-cookie-service';
import { CreateShipmentComponent } from './components/create-shipment/create-shipment.component';
import { ShipmentsTableComponent } from './components/shipments-table/shipments-table.component';
import { CountryTableComponent } from './components/country-table/country-table.component';
import { CreateCountryComponent } from './components/create-country/create-country.component';
import { CountryFormComponent } from './components/country-form/country-form.component';
import { UserInfoFormComponent } from './components/user-info-form/user-info-form.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    AccountComponent,
    MainComponent,
    SettingsComponent,
    NotFoundComponent,
    LandingComponent,
    RegistrationFormComponent,
    LoadingComponent,
    ShipmentsComponent,
    SearchComponent,
    SearchFilterPipe,
    ShipmentsTableComponent,
    CreateShipmentComponent,
    CountryTableComponent,
    CreateCountryComponent,
    CountryFormComponent,
    UserInfoFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ColorSketchModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    ColorPickerModule,
    CollapseModule.forRoot(),
    HttpClientModule,
    AuthModule.forRoot({
      ...env.auth,
      httpInterceptor: {
        allowedList: [],
      },
    }),
  ],

  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true,
    },
    CookieService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
