import {
  Component,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
} from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { CountryCost } from 'src/app/models/country-cost.model';

@Component({
  selector: 'app-country-table',
  templateUrl: './country-table.component.html',
  styleUrls: ['./country-table.component.css'],
})
export class CountryTableComponent {
  @Input() searchKey: string = '';
  @Input() countries: CountryCost[] = [];
  @Output() countryUpdated = new EventEmitter();

  country: CountryCost;

  modalRef?: BsModalRef;

  constructor(private modalService: BsModalService) {}

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  onCountryClicked(countryCost: CountryCost) {
    this.country = countryCost;
  }

  onButtonClicked(updatedCountryCost: CountryCost) {
    this.countryUpdated.emit(updatedCountryCost);
  }
}
