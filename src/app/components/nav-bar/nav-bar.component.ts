import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
})
export class LandingComponent {
  isCollapsed = true;

  constructor(
    public sessionService: SessionService,
    public auth: AuthService,
    private readonly router: Router
  ) {}

  get onAccountPage() {
    return this.router.url === '/account';
  }
}
