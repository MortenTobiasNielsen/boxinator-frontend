import {
  Component,
  EventEmitter,
  HostListener,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { CountryCost } from 'src/app/models/country-cost.model';

@Component({
  selector: 'app-create-country',
  templateUrl: './create-country.component.html',
  styleUrls: ['./create-country.component.css'],
})
export class CreateCountryComponent implements OnInit {
  @Output() countryCreated = new EventEmitter();

  public screenWidth: number;
  public minScreenWidth: number = 510;

  modalRef!: BsModalRef;

  constructor(private readonly modalService: BsModalService) {}

  ngOnInit(): void {
    this.screenWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.screenWidth = window.innerWidth;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  onButtonClicked(newCountryCost: CountryCost) {
    this.countryCreated.emit(newCountryCost);
  }
}
