import { HttpErrorResponse } from '@angular/common/http';
import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@auth0/auth0-angular';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CountryCost } from 'src/app/models/country-cost.model';
import { NewShipment } from 'src/app/models/shipment.model';
import { Tier } from 'src/app/models/tier.model';
import { SessionService } from 'src/app/services/session.service';
import { ShipmentService } from 'src/app/services/shipment.service';
import { PATTERNS } from 'src/app/utils/constants';

@Component({
  selector: 'app-create-shipment',
  templateUrl: './create-shipment.component.html',
  styleUrls: ['./create-shipment.component.css'],
})
export class CreateShipmentComponent implements OnInit {
  @Input() tiers: Tier[];
  @Input() countries: CountryCost[];
  @Output() shipmentCreated = new EventEmitter();

  modalRef!: BsModalRef;
  shipmentForm = new FormGroup({
    receiverName: new FormControl('', [
      Validators.required,
      Validators.pattern(PATTERNS.NAME),
      Validators.minLength(2),
    ]),
    receiverAddress: new FormControl('', [
      Validators.required,
      Validators.pattern(PATTERNS.ADDRESS),
      Validators.minLength(10),
    ]),
    hexColor: new FormControl('#000000', [Validators.required]),
    tierId: new FormControl('', [Validators.required]),
    countryId: new FormControl('', [Validators.required]),
  });

  flatFee: number;
  tierIdChosen: number = -1;
  CountryIdChosen: number = -1;
  shipmentCost = 0;

  isAuthenticated = true;

  submitted = false;

  public screenWidth: number;
  public minScreenWidth: number = 510;

  constructor(
    private readonly modalService: BsModalService,
    private readonly shipmentService: ShipmentService,
    public readonly sessionService: SessionService,
    public auth: AuthService
  ) {
    this.getFlatFee();
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.screenWidth = window.innerWidth;
  }

  ngOnInit(): void {
    this.screenWidth = window.innerWidth;

    this.auth.isAuthenticated$.subscribe((isAuthenticated: boolean) => {
      if (!isAuthenticated) {
        this.isAuthenticated = false;
        this.shipmentForm.addControl(
          'senderEmail',
          new FormControl('', [
            Validators.required,
            Validators.pattern(PATTERNS.EMAIL),
          ])
        );
      }
    });

    this.shipmentForm.get('tierId')!.valueChanges.subscribe((tierId) => {
      this.tierIdChosen = tierId;

      if (this.CountryIdChosen != -1) {
        this.shipmentCost =
          this.flatFee +
          this.countries[this.CountryIdChosen].multiplier *
            this.tiers[tierId].weight;
      }
    });

    this.shipmentForm.get('countryId')!.valueChanges.subscribe((countryId) => {
      this.CountryIdChosen = countryId;

      if (this.tierIdChosen != -1) {
        this.shipmentCost =
          this.flatFee +
          this.countries[countryId].multiplier *
            this.tiers[this.tierIdChosen].weight;
      }
    });
  }

  get formControls() {
    return this.shipmentForm.controls;
  }

  getFlatFee() {
    this.shipmentService.GetFlatFee().subscribe((flatFee: number) => {
      this.flatFee = flatFee;
    });
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  save() {
    this.submitted = true;

    if (this.shipmentForm.status === 'INVALID') {
      return;
    }

    const newShipment: NewShipment = {
      receiverName: this.shipmentForm.value.receiverName,
      address: this.shipmentForm.value.receiverAddress,
      hexColor: this.shipmentForm.value.hexColor,
      tierId: this.shipmentForm.value.tierId,
      country: this.countries[this.shipmentForm.value.countryId - 1].name,
      price: this.shipmentCost,
      userEmail: '',
    };

    if (this.isAuthenticated) {
      this.auth.user$.subscribe((profile) => {
        newShipment.userEmail = profile?.email!;
        this.createShipment(newShipment);
      });
    } else {
      newShipment.userEmail = this.shipmentForm.value.senderEmail;
      this.createShipment(newShipment);
    }
  }

  createShipment(newShipment: NewShipment) {
    this.shipmentService.create(newShipment).subscribe(
      () => {
        alert('Shipment Created');
        this.modalRef?.hide();
        this.shipmentCreated.emit();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
}
