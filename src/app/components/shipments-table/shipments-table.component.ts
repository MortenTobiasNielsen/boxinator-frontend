import {
  Component,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
} from '@angular/core';
import { Shipment } from 'src/app/models/shipment.model';
import { SessionService } from 'src/app/services/session.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ShipmentService } from 'src/app/services/shipment.service';
import { ShipmentHistory } from 'src/app/models/shipment-history';
import { ShipmentStatus } from 'src/app/models/shipment-status.model';
import { SHIPMENT_TYPES } from 'src/app/utils/constants';

@Component({
  selector: 'app-shipments-table',
  templateUrl: './shipments-table.component.html',
  styleUrls: ['./shipments-table.component.css'],
})
export class ShipmentsTableComponent {
  @Input() shipments: Shipment[];
  @Input() shipmentStatuses: ShipmentStatus[];
  @Input() searchKey: string = '';
  @Output() shipmentType = new EventEmitter();
  @Output() shipmentChanged = new EventEmitter();

  shipmentIndex = 0;
  shipmentHistories: ShipmentHistory[] = [];
  shipment: Shipment;

  modalRef!: BsModalRef;

  shipment_types = SHIPMENT_TYPES;

  focused = this.shipment_types.ACTIVE;

  constructor(
    private readonly modalService: BsModalService,
    public readonly sessionService: SessionService,
    private readonly shipmentService: ShipmentService
  ) {}

  handleCancelShipment() {
    if (confirm('Are you sure you want to cancel this shipment')) {
      let shipmentToCancel = {
        ...this.shipments[this.shipmentIndex],
        shipmentStatusId: 5,
      };
      this.shipmentService.update(shipmentToCancel).subscribe(
        () => {
          this.shipmentChanged.emit(this.focused);
          this.modalRef?.hide();
        },
        () => {
          alert("The shipment couldn't be cancelled - please try again later");
        }
      );
    }
  }

  shipmentStatusChange(shipmentStatusId: string, index: number) {
    if (
      confirm(
        'Sure you want to change shipment status to ' +
          this.shipmentStatuses[+shipmentStatusId - 1].status
      )
    ) {
      let shipmentToChange = {
        ...this.shipments[index],
        shipmentStatusId: +shipmentStatusId,
      };
      this.shipmentService.update(shipmentToChange).subscribe(
        () => {
          this.shipmentChanged.emit(this.focused);
        },
        () => {
          alert("The shipment couldn't be changed - please try again later");
        }
      );
    }
  }

  onActiveClicked() {
    this.shipmentType.emit(this.shipment_types.ACTIVE);
    this.focused = this.shipment_types.ACTIVE;
  }

  onCompletedClicked() {
    this.shipmentType.emit(this.shipment_types.COMPLETED);
    this.focused = this.shipment_types.COMPLETED;
  }

  onCancelledClicked() {
    this.shipmentType.emit(this.shipment_types.CANCELLED);
    this.focused = this.shipment_types.CANCELLED;
  }

  openModal(
    template: TemplateRef<any>,
    shipmentId: number,
    shipmentIndex: number
  ) {
    this.shipmentIndex = shipmentIndex;

    this.shipmentService
      .GetHistory(shipmentId)
      .subscribe((shipmentHistories: ShipmentHistory[]) => {
        this.shipmentHistories = [];

        for (let shipmentHistory of shipmentHistories) {
          shipmentHistory.status =
            this.shipmentStatuses[shipmentHistory.shipmentStatusId - 1].status;
          shipmentHistory.statusChangeDateTime = new Date(
            shipmentHistory.statusChangeDateTime
          );

          shipmentHistory.statusChangeDateTimeFormatted = this.formatDate(
            shipmentHistory.statusChangeDateTime
          );

          this.shipmentHistories.push(shipmentHistory);
        }

        this.modalRef = this.modalService.show(template);
      });
  }

  formatDate(shipmentDate: Date): string {
    const year = shipmentDate.getFullYear();
    const monthNumber = shipmentDate.getMonth() + 1;
    const dayNumber = shipmentDate.getDate();
    const hourNumber = shipmentDate.getHours();
    const minuteNumber = shipmentDate.getMinutes();

    const monthFormatted = this.makeTwoDigit(monthNumber);
    const dayFormatted = this.makeTwoDigit(dayNumber);
    const hourFormatted = this.makeTwoDigit(hourNumber);
    const minuteFormatted = this.makeTwoDigit(minuteNumber);

    return `${year}-${monthFormatted}-${dayFormatted} ${hourFormatted}:${minuteFormatted}`;
  }

  makeTwoDigit(digit: number) {
    if (digit < 10) {
      return '0' + digit;
    }

    return digit;
  }
}
