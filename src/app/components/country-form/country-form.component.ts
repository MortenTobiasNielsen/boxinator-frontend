import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { CountryCost } from 'src/app/models/country-cost.model';
import { PATTERNS } from 'src/app/utils/constants';

@Component({
  selector: 'app-country-form',
  templateUrl: './country-form.component.html',
  styleUrls: ['./country-form.component.css'],
})
export class CountryFormComponent implements OnInit {
  @Input() heading: string;
  @Input() buttonName: string;
  @Input() modalRef: BsModalRef;
  @Input() country: CountryCost;
  @Output() buttonClicked = new EventEmitter();

  countryForm = new FormGroup({
    countryId: new FormControl('', []),
    name: new FormControl('', [
      Validators.required,
      Validators.pattern(PATTERNS.NAME),
    ]),
    multiplier: new FormControl('', [
      Validators.required,
      Validators.pattern(/d/),
    ]),
  });

  constructor() {}

  ngOnInit(): void {
    if (this.country) {
      this.countryForm.patchValue(this.country);
    }
  }

  onClickHandler() {
    this.buttonClicked.emit(this.countryForm.value);
  }
}
