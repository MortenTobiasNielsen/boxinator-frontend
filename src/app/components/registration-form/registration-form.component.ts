import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { CountryCostService } from 'src/app/services/country-cost.service';
import { PATTERNS } from 'src/app/utils/constants';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css'],
})
export class RegistrationFormComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  loading = false;
  maxDate = new Date();
  registrationError = false;

  Country: any[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private sessionService: SessionService,
    private countryService: CountryCostService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.maxDate.setFullYear(this.maxDate.getFullYear() - 10);
  }

  ngOnInit() {
    this.getAllCountry();
    this.registerForm = this.formBuilder.group(
      {
        firstName: [
          '',
          [
            Validators.required,
            Validators.pattern(PATTERNS.NAME),
            Validators.minLength(2),
          ],
        ],
        lastName: [
          '',
          [
            Validators.required,
            Validators.pattern(PATTERNS.NAME),
            Validators.minLength(2),
          ],
        ],
        dob: ['', [Validators.required, Validators.pattern(PATTERNS.DOB)]],
        email: ['', [Validators.required, Validators.pattern(PATTERNS.EMAIL)]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', [Validators.required]],
        countryOfResidence: ['', [Validators.required]],
        address: [
          '',
          [
            Validators.required,
            Validators.pattern(PATTERNS.ADDRESS),
            Validators.minLength(10),
          ],
        ],
        zipCode: [
          '',
          [
            // TODO: Are we sure a zip code should always be a number?
            Validators.required,
            Validators.pattern('^[0-9]*$'),
            Validators.minLength(4),
          ],
        ],
        contactNumber: [
          '',
          [
            Validators.required,
            Validators.pattern('^[0-9]*$'),
            Validators.minLength(8),
          ],
        ],
      },
      {
        validator: [this.MustMatch('password', 'confirmPassword')],
      }
    );
  }

  getAllCountry() {
    this.countryService.getAll().subscribe((data) => {
      this.Country = data;
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.loading = true;
    // TODO check for email in boxinatorApi
    this.sessionService.register(this.registerForm.value).subscribe(
      (data) => {
        if (data !== null || data !== undefined) {
          this.sessionService
            .createUserInApi(this.registerForm.value)
            .subscribe((data) => {
              this.sessionService.loginWithRedirect();
            });
        }
      },
      (error) => {
        if (error.error.message === 'The user already exists.') {
          this.registrationError = true;
        }
      }
    );
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }
}
