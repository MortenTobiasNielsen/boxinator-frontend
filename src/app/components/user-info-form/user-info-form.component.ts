import { formatDate } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Account } from 'src/app/models/account.model';
import { CountryCost } from 'src/app/models/country-cost.model';
import { PATTERNS } from 'src/app/utils/constants';

@Component({
  selector: 'app-user-info-form',
  templateUrl: './user-info-form.component.html',
  styleUrls: ['./user-info-form.component.css'],
})
export class UserInfoFormComponent implements OnInit {
  @Input() buttonName: string;
  @Input() accountInfo: Account;
  @Input() countries: CountryCost[] = [];
  @Output() buttonClicked = new EventEmitter();

  userInfoForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.userInfoForm = this.formBuilder.group({
      firstName: new FormControl('', [
        Validators.required,
        Validators.pattern(PATTERNS.NAME),
        Validators.minLength(2),
      ]),
      lastName: new FormControl('', [
        Validators.required,
        Validators.pattern(PATTERNS.NAME),
        Validators.minLength(2),
      ]),
      dateOfBirth: new FormControl('', [
        Validators.required,
        Validators.pattern(PATTERNS.DOB),
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(PATTERNS.EMAIL),
      ]),
      countryId: new FormControl('', [Validators.required]),
      address: new FormControl('', [
        Validators.required,
        Validators.pattern(PATTERNS.ADDRESS_NO_ZIP),
        Validators.minLength(10),
      ]),
      zipCode: new FormControl('', [
        // TODO: Are we sure a zip code should always be a number?
        Validators.required,
        Validators.pattern('^[0-9]*$'),
        Validators.minLength(4),
      ]),
      phoneNumber: new FormControl('', [
        Validators.required,
        Validators.pattern('^[0-9]*$'),
        Validators.minLength(8),
      ]),
    });

    if (this.accountInfo) {
      this.userInfoForm.patchValue(this.accountInfo);
      this.userInfoForm.controls.dateOfBirth.setValue(
        formatDate(this.accountInfo.dateOfBirth, 'yyyy-MM-dd', 'en')
      );

      const zipCode = this.accountInfo.address
        .split(',')[1]
        .split(' ')[1]
        .trim();
      const address = this.accountInfo.address.replace(', ' + zipCode, ',');

      this.userInfoForm.controls.zipCode.setValue(zipCode);
      this.userInfoForm.controls.address.setValue(address);
    } else {
      this.userInfoForm.addControl(
        'password',
        new FormControl('', [Validators.required, Validators.minLength(6)])
      );

      this.userInfoForm.addControl(
        'confirmPassword',
        new FormControl('', [Validators.required])
      );

      this.userInfoForm.validator = this.comparisonValidator(this.userInfoForm);
    }
  }

  comparisonValidator(formGroup: FormGroup): any {
    if (this.accountInfo) {
      return;
    }

    const control = formGroup.controls['password'];
    const matchingControl = formGroup.controls['confirmPassword'];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      return;
    }
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }

  get formControls() {
    return this.userInfoForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.userInfoForm.status === 'INVALID') {
      return;
    }

    this.buttonClicked.emit(this.userInfoForm.value);
  }
}
