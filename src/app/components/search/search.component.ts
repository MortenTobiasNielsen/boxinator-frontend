import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent {
  searchKey = '';
  @Output() emitSearchKeyValue = new EventEmitter();

  emitSearchKey() {
    this.emitSearchKeyValue.emit(this.searchKey);
  }
}
