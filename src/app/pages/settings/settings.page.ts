import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { CountryCost } from 'src/app/models/country-cost.model';
import { CountryCostService } from 'src/app/services/country-cost.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.css'],
})
export class SettingsComponent {
  countries: CountryCost[] = [];
  searchKey = '';
  isLoading = true;

  constructor(private countryCostService: CountryCostService) {
    this.getCountries();
  }

  getCountries() {
    this.countryCostService.getAll().subscribe(
      (countries: CountryCost[]) => {
        this.countries = countries;
        this.isLoading = false;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  updateCountries() {
    this.getCountries();
  }

  setSearchKey(event: any) {
    this.searchKey = event;
  }

  createNewCountry(newCountryCost: CountryCost) {
    this.countryCostService
      .create(newCountryCost.name, newCountryCost.multiplier)
      .subscribe(
        () => {
          this.getCountries();
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
  }

  updateCountry(updatedCountryCost: CountryCost) {
    this.countryCostService.Update(updatedCountryCost).subscribe(
      () => {
        this.getCountries();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
}
