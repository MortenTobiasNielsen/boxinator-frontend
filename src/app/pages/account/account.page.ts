import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { CountryCost } from 'src/app/models/country-cost.model';
import { Account } from 'src/app/models/account.model';
import { AccountService } from 'src/app/services/account.service';
import { CountryCostService } from 'src/app/services/country-cost.service';
import { Router } from '@angular/router';
import { User } from '@auth0/auth0-spa-js';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.css'],
})
export class AccountComponent implements OnInit {
  accountInfo: Account;
  countries: CountryCost[] = [];
  isLoading: boolean = true;

  constructor(
    public auth: AuthService,
    private readonly countryCostService: CountryCostService,
    private readonly accountService: AccountService
  ) {
    this.countryCostService.getAll().subscribe(
      (countries: CountryCost[]) => {
        this.countries = countries;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  ngOnInit(): void {
    // TODO: Should this be done on user login and application refresh?
    this.auth.user$.subscribe((profile) => {
      this.updateAccountInfo(profile);
    });
  }

  updateAccountInfo(profile: User | null | undefined) {
    this.accountService.getByEmail(profile?.email!).subscribe(
      (accountInfo: Account) => {
        this.accountInfo = accountInfo;
        this.accountInfo.dateOfBirth = new Date(this.accountInfo.dateOfBirth);
        this.accountInfo.email = profile?.email!;

        this.isLoading = false;
      },
      () => {
        alert(
          'We unfortunately could not find your account information - please try again later'
        );
      }
    );
  }

  onUpdateAccount(accountInfo: Account) {
    const temp = accountInfo.address.split(',');
    accountInfo.address = `${temp[0]}, ${accountInfo.zipCode}${temp[1]}`;

    this.accountInfo = { ...accountInfo, userId: this.accountInfo.userId };

    this.accountService
      .Update(this.accountInfo.userId, this.accountInfo)
      .subscribe(
        () => {
          alert('Updated');
        },
        (error) => {
          console.log(error.message);
        }
      );
  }
}
