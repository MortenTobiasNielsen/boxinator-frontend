import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CountryCost } from 'src/app/models/country-cost.model';
import { CountryCostService } from 'src/app/services/country-cost.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.css'],
})
export class RegisterComponent implements OnInit {
  countries: CountryCost[] = [];

  constructor(
    private readonly countryCostService: CountryCostService,
    private readonly sessionService: SessionService
  ) {
    this.countryCostService.getAll().subscribe(
      (countries: CountryCost[]) => {
        this.countries = countries;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  ngOnInit(): void {}

  onRegisterAccount(formValues: any) {
    this.sessionService.register(formValues).subscribe(
      (data) => {
        if (data !== null || data !== undefined) {
          this.sessionService.createUserInApi(formValues).subscribe(
            () => {
              this.sessionService.loginWithRedirect();
            },
            () => {
              console.log('Error while registering user');
            }
          );
        }
      },
      () => {
        console.log('Error while registering user');
      }
    );
  }
}
