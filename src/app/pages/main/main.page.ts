import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.css'],
})
export class MainComponent implements OnInit {
  constructor( private readonly router: Router,) {}

  ngOnInit(): void {}

  onClick(): void {
      this.router.navigate(['shipments'])
  }
}
