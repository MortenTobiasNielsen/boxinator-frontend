import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.page.html',
  styleUrls: ['./not-found.page.css'],
})
export class NotFoundComponent implements OnInit {
  constructor( private readonly router: Router) {}

  ngOnInit(): void {}
  onClick(): void {
    this.router.navigate(['shipments'])
}
}
