import { Component, OnInit } from '@angular/core';
import { ShipmentService } from '../../services/shipment.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { CountryCostService } from 'src/app/services/country-cost.service';
import { Tier } from 'src/app/models/tier.model';
import { UtilsService } from 'src/app/services/utils.service';
import { CountryCost } from 'src/app/models/country-cost.model';
import { Shipment } from 'src/app/models/shipment.model';
import { ShipmentStatus } from 'src/app/models/shipment-status.model';
import { SessionService } from 'src/app/services/session.service';
import { AuthLevels } from 'src/app/utils/enums';
import { SHIPMENT_TYPES } from 'src/app/utils/constants';
import { HttpErrorResponse } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-shipments',
  templateUrl: './shipments.page.html',
  styleUrls: ['./shipments.page.css'],
})
export class ShipmentsComponent implements OnInit {
  modalRef?: BsModalRef;
  searchKey = '';
  isLoading = true;

  shipments: Shipment[] = [];
  activeShipments: Shipment[] = [];
  completedShipments: Shipment[] = [];
  cancelledShipments: Shipment[] = [];

  tiers: Tier[] = [];
  shipmentStatuses: ShipmentStatus[] = [];
  countries: CountryCost[] = [];

  userEmail: string;

  constructor(
    private readonly auth: AuthService,
    private readonly shipmentService: ShipmentService,
    private readonly countryCostService: CountryCostService,
    public readonly sessionService: SessionService,
    private readonly utilsService: UtilsService,
    private readonly cookieService: CookieService
  ) {}

  ngOnInit(): void {
    // I have not been able to find a good way to do this, so for now I have implemented it as a string of calls, where one calls the other.
    // This is most certainly not the most optimal or prettiest, but it seems to work.
    this.getTiers();
    if (this.cookieService.get('userProfile') === undefined) {
      this.sessionService.fetchUserProfile();
    }
  }

  getTiers() {
    this.utilsService.GetTiers().subscribe(
      (tiers: Tier[]) => {
        this.tiers = tiers;

        this.getCountries();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  getCountries() {
    this.countryCostService.getAll().subscribe(
      (countries: CountryCost[]) => {
        this.countries = countries;

        this.getShipmentStatuses();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  getShipmentStatuses(): void {
    this.utilsService.GetShipmentStatuses().subscribe(
      (shipmentStatuses: ShipmentStatus[]) => {
        for (let shipmentStatus of shipmentStatuses) {
          shipmentStatus.status =
            shipmentStatus.status[0].toUpperCase() +
            shipmentStatus.status.substring(1).toLowerCase();
        }
        this.shipmentStatuses = shipmentStatuses;
        this.getUserEmail();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  getUserEmail() {
    this.auth.user$.subscribe((profile) => {
      this.userEmail = profile?.email!;
      this.getActiveShipments();
    });
  }

  getCompletedShipments() {
    if (this.userEmail) {
      this.shipmentService.GetAllCompleted(this.userEmail).subscribe(
        (shipments: Shipment[]) => {
          this.completedShipments = shipments;
          this.shipments = shipments;

          this.formatShipments();
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    }
  }

  getCancelledShipments() {
    if (this.userEmail) {
      this.shipmentService.GetAllCancelled(this.userEmail).subscribe(
        (shipments: Shipment[]) => {
          this.cancelledShipments = shipments;
          this.shipments = shipments;

          this.formatShipments();
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    }
  }

  getActiveShipments() {
    if (this.userEmail) {
      this.shipmentService.GetAllActive(this.userEmail).subscribe(
        (shipments: Shipment[]) => {
          this.activeShipments = shipments;
          this.shipments = shipments;

          this.formatShipments();
          this.isLoading = false;
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    } else {
      this.isLoading = false;
    }
  }

  formatShipments(): void {
    for (let shipment of this.shipments) {
      shipment.tier = this.tiers[shipment.tierId].name;
    }

    for (let shipment of this.shipments) {
      shipment.shipmentStatuses = [];

      if (this.sessionService.userSession.authLevel < AuthLevels.ADMIN) {
        shipment.shipmentStatuses.push(
          this.shipmentStatuses[shipment.shipmentStatusId - 1]
        );
      } else {
        for (let shipmentStatus of this.shipmentStatuses) {
          shipment.shipmentStatuses.push(shipmentStatus);
        }
      }
    }
  }

  setSearchKey(event: string) {
    this.searchKey = event;
  }

  setShipmentType(shipmentType: string) {
    if (shipmentType === SHIPMENT_TYPES.ACTIVE) {
      this.getActiveShipments();
    } else if (shipmentType === SHIPMENT_TYPES.COMPLETED) {
      this.getCompletedShipments();
    } else {
      this.getCancelledShipments();
    }
  }

  triggerShipmentUpdate(shipmentType: string) {
    if (shipmentType === SHIPMENT_TYPES.ACTIVE) {
      this.getActiveShipments();
    } else if (shipmentType === SHIPMENT_TYPES.COMPLETED) {
      this.getCompletedShipments();
    } else {
      this.getCancelledShipments();
    }
  }
}
