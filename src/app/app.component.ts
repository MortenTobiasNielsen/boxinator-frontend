import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { SessionService } from './services/session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'boxinator-frontend';
  constructor(
    public sessionService: SessionService,
    public auth: AuthService
  ) {}

  ngOnInit(): void {
    this.sessionService.fetchManagementToken();
    this.sessionService.fetchBoxinatorWebApiToken();
    this.sessionService.fetchUserProfile();
  }
}
