# BoxinatorFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli)

Deployed frontend link: [https://boxinator-ddmn-dk.herokuapp.com/](https://boxinator-ddmn-dk.herokuapp.com/)

User Manual: https://docs.google.com/document/d/1Yc8fBr55GQjSWia-17vx0cYLSzhNo4my/

System uses Auth0 for user authentication. Auth0 2FA is currently turned off, but was successfully tested and can be turned on at any time.

## User functionalities:

Guest functionality: click "MORE INFO" button and create a shipment with desired email. It is possible to see shipment history if registered with that email later on.

Registered user functionality: once registered, a customer can see shipments history, see shipment details and cancel a shipment

Administrator functionality - see all shipments of all users (also guests), change their statuses, add and/or change countries and their multipliers.

Admin functionality can be tested by logging in with credentials: 

login: AdminTest@boxinator.com

password: AdminTest@boxinator.com 

## Running system locally:

Clone the repository and run  `npm install` to install necessary packages.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
